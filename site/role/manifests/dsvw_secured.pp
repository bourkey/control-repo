class role::dsvw_secured (
  String $app_name = 'dsvw_be',
  String $waf_name = 'modsecurity_be',
  String $app_backend_ip = '127.0.0.1',
  String $app_backend_port = '8002',
  String $waf_backend_ip = 'waf.internal.salesengineer.cloud',
  String $waf_backend_port = '8002',
  ){

  #This role would be made of all the profiles that need to be included to make a webserver work
  #All roles should include the base profile
  include profile::base
  # include profile::hardenos
  include profile::docker_centos
  include profile::letsencrypt
  include profile::haproxy

  docker::image { 'appsecco/dsvw':
    image_tag => 'latest'
  }

  docker::run { 'dsvw':
    image => 'appsecco/dsvw',
    ports => ["${app_backend_port}:8000"],
  }
  # Configure Application backend
  haproxy::backend { $app_name :
    options => {
      'mode'    => 'http',
      'option'  => [
        'forwardfor',
      ],
      'balance' => 'roundrobin',
    },
  }

  haproxy::balancermember { 'dsvw_bm_01':
    listening_service => $app_name,
    server_names      => $facts['networking']['hostname'],
    ipaddresses       => $app_backend_ip,
    ports             => $app_backend_port,
    options           => 'check',
  }

  # Configure WAF Backend
  haproxy::backend { $waf_name :
    options => {
      'mode'    => 'http',
      'option'  => [
        'forwardfor',
      ],
      'balance' => 'roundrobin',
    },
  }

  haproxy::balancermember { 'waf_bm_01':
    listening_service => $waf_name,
    server_names      => 'waf.interal',
    ipaddresses       => $waf_backend_ip,
    ports             => $waf_backend_port,
    options           => 'check',
  }


  # Configure Inbound Front Ends to direct to WAF First
  haproxy::frontend { 'http_frontend':
    bind    => {
      ':80'   => [],
    },
    mode    => 'http',
    options => [
      { 'default_backend' => $waf_name },
      { 'reqadd' => 'X-Forwarded-Proto:\ http' },
      { 'redirect' => 'scheme https if ! { ssl_fc }'},
      ],
  }

  haproxy::frontend { 'https_frontend':
    bind    => {
      ':443'      => ['ssl', 'crt', "/etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchainkey.pem", ],
    },
    mode    => 'http',
    options => {
      'default_backend' => $waf_name,
      'reqadd'          => 'X-Forwarded-Proto:\ https',
      #'use_backend'     => "${waf_name} if { ssl_fc_sni ${facts['networking']['fqdn']} } ",
      'http-response'   => [
        'set-header Strict-Transport-Security max-age=315360010',
        'set-header X-Frame-Options DENY',
        'set-header X-Content-Type-Options nosniff',
      ],
      'rspidel'         =>  ['^Server:.$', '^X-Powered-By:.*$',],
    },
  }
  #Configure Front End to redirect cleaned traffic to Application
  haproxy::frontend { 'app_frontend':
    bind    => {
      ':8002'   => [],
    },
    mode    => 'http',
    options => [
      { 'default_backend' => $app_name },
      { 'reqadd' => 'X-Forwarded-Proto:\ http' },
      ],
  }
}
