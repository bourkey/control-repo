class role::modsecurity (
  String $juiceshop_backend = 'http://juiceshop-secured.internal.salesengineer.cloud:8001',
  String $dvwa_backend = 'http://dvwa-secured.internal.salesengineer.cloud:8002',
  ){
  #This role would be made of all the profiles that need to be included to make a webserver work
  #All roles should include the base profile
  include profile::base
  # include profile::hardenos
  include profile::docker_centos

  docker::image { 'franbuehler/modsecurity-crs-rp':
    image_tag => 'v3.1'
  }
  # juiceshop
  docker::run { 'juiceshop-modsec':
    image => 'franbuehler/modsecurity-crs-rp:v3.1',
    ports => ['8001:8001'],
    env   => [
      'PORT=8001',
      'PARANOIA=1',
      'ANOMALYIN=5',
      'ANOMALYOUT=4',
      "BACKEND=${juiceshop_backend}",
    ]
  }
  # dvwa 
  docker::run { 'dvwa-modsec':
    image => 'franbuehler/modsecurity-crs-rp:v3.1',
    ports => ['8002:8001'],
    env   => [
      'PORT=8001',
      'PARANOIA=1',
      'ANOMALYIN=5',
      'ANOMALYOUT=4',
      "BACKEND=${dvwa_backend}",
    ]
  }
}
