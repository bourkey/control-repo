class role::dsvw (
  # declare variables
  String $backend_name = 'dsvw_be',
  String $backend_ip = '127.0.0.1',
  String $backend_port = '8000',
  ){
    #All roles should include the base profile
  include profile::base
  # include profile::hardenos
  include profile::docker_centos
  include profile::letsencrypt
  include profile::haproxy

  docker::image { 'appsecco/dsvw':
    image_tag => 'latest'
  }

  docker::run { 'dsvw':
    image => 'appsecco/dsvw',
    ports => ["${backend_port}:8000"],
  }
  haproxy::backend { $backend_name :
    options => {
      'mode'    => 'http',
      'option'  => [
        'forwardfor',
      ],
      'balance' => 'roundrobin',
    },
  }

  haproxy::balancermember { 'dsvw_bm':
    listening_service => $backend_name,
    server_names      => $facts['networking']['hostname'],
    ipaddresses       => $backend_ip,
    ports             => $backend_port,
    #options           => 'check',
  }

  haproxy::frontend { 'http_frontend':
    bind    => {
      ':80'   => [],
    },
    mode    => 'http',
    options => [
      { 'default_backend' => $backend_name },
      { 'reqadd' => 'X-Forwarded-Proto:\ http' },
      { 'redirect' => 'scheme https if ! { ssl_fc }'},
      ],
  }

  haproxy::frontend { 'https_frontend':
    bind    => {
      ':443'      => ['ssl', 'crt', "/etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchainkey.pem", ],
    },
    mode    => 'http',
    options => {
      'default_backend' => $backend_name,

      'reqadd'          => 'X-Forwarded-Proto:\ https',
     # 'use_backend'     => "${backend_name} if { ssl_fc_sni ${facts['networking']['fqdn']} } ",
      'http-response'   => [
        'set-header Strict-Transport-Security max-age=315360010',
        'set-header X-Frame-Options DENY',
        'set-header X-Content-Type-Options nosniff',
      ],
      'rspidel'         =>  ['^Server:.$', '^X-Powered-By:.*$',],
    },
  }
}
