class profile::letsencrypt {
  ## include epel
  include epel
  ## Global Defaults
  class { ::letsencrypt:
    configure_epel         => true,
    email                  => 'admin@salesengineer.cloud',
  }
  ## create cert based on fqdn
  letsencrypt::certonly { $facts['networking']['fqdn']:
    domains              => [ $facts['networking']['fqdn'], ],
    manage_cron          => true,
    cron_before_command  => 'systemctl stop haproxy',
    cron_success_command => 'systemctl start haproxy',
    suppress_cron_output => true,
  }
}

