class profile::haproxy (){

  exec { 'concat_keys':
    command         => "/usr/bin/cat /etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchain.pem /etc/letsencrypt/live/${facts['networking']['fqdn']}/privkey.pem > /etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchainkey.pem ",
    onlyif          =>  "/usr/bin/test ! -f /etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchainkey.pem",
  }

  exec { 'chmod_keys':
    command         => "/usr/bin/chmod 400 /etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchainkey.pem ",
    onlyif          =>  "/usr/bin/test ! -f /etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchainkey.pem",
  }
  #TODO Move to RSYSLOG Module
  file { 'rsyslog_conf':
    path     =>  '/etc/rsyslog.d/haproxy.conf',
    ensure   => present,
    content  => inline_template( "if \$programname startswith 'haproxy' then -/var/log/haproxy/haproxy.log", "\n", "& ~" ),
  }
  exec { 'reload_rsyslog':
    command         => "/usr/bin/systemctl restart rsyslog",
    onlyif          =>  "/usr/bin/test -f /etc/rsyslog.d/haproxy.conf",
  }

  # Declare haproxy base class with configuration options
  class { '::haproxy':
    enable           => true,
    global_options   => {
      'log'                       => "${::ipaddress} local0",
      'chroot'                    => '/var/lib/haproxy',
      'pidfile'                   => '/var/run/haproxy.pid',
      'maxconn'                   => '4000',
      'user'                      => 'haproxy',
      'group'                     => 'haproxy',
      'daemon'                   => '',
      'tune.ssl.default-dh-param' => '4096',
      'ssl-default-bind-ciphers'  => 'EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA384:EECDH+ECDSA+SHA256:EECDH+aRSA+SHA384:EECDH+aRSA+SHA256:EECDH+aRSA+RC4:EECDH:EDH+aRSA:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS:!RC4',
      'ssl-default-bind-options'  => 'no-sslv3 no-tlsv10 no-tls-tickets'
    },

    defaults_options => {
      'mode'    => 'http',
      'log'     => 'global',
      'option'  => ['redispatch', 'httplog', 'dontlognull'],
      'retries' => '3',
      'timeout' => [
        'http-request 10s',
        'queue 1m',
        'connect 10s',
        'client 1m',
        'server 1m',
        'check 10s',
      ],
      'maxconn' => '3000',
    },
  }
}
