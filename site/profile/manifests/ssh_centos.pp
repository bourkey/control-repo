class profile::ssh_centos (){

  class { ::ssh:
    ssh_key_type => 'ecdsa-sha2-nistp256',
    sshd_config_hostkey => ['/etc/ssh/ssh_host_ecdsa_key',],
  }

}
