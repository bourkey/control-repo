# Installs a Nessus scanner
class profile::nessus::scanner {
  file {"test profile":
    path => '/tmp/nessus_scanner',
    ensure => "directory",
  }
}
