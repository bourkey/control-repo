class profile::nessus::agent {
  file {'test profile':
    path => '/tmp/nessus_agent',
    ensure => 'directory',
  }
}
