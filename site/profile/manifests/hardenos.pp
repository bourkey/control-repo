class profile::hardenos(
  Boolean $enable_ipv4_forwarding = false,
  Array $unwanted_packages = [],
  Array $ignore_users = ['chrony','systemd-network', 'dbus', 'polkitd', 'centos', 'ec2-user', 'rpc', 'rpcuser', 'sshd', 'postfix' ],
) {
  class { '::os_hardening':
    pe_environment => true,
    enable_ipv4_forwarding => $enable_ipv4_forwarding,
    unwanted_packages => $unwanted_packages,
    ignore_users => $ignore_users,
  }
}
