# A Puppet Control Repository #

Here's a visual representation of the structure of this repository:

```
control-repo/
├── data/                                 # Hiera data directory.
│   ├── nodes/                            # Node-specific data goes here.
│   └── common.yaml                       # Common data goes here.
├── manifests/
│   └── site.pp                           # The "main" manifest that contains a default node definition.
├── scripts/
│   ├── code_manager_config_version.rb    # A config_version script for Code Manager.
│   ├── config_version.rb                 # A config_version script for r10k.
│   └── config_version.sh                 # A wrapper that chooses the appropriate config_version script.
├── site/                                 # This directory contains site-specific modules and is added to $modulepath.
│   ├── profile/                          # The profile module.
│   └── role/                             # The role module.
├── LICENSE
├── Puppetfile                            # A list of external Puppet modules to deploy with an environment.
├── README.md
├── environment.conf                      # Environment-specific settings. Configures the modulepath and config_version.
└── hiera.yaml                            # Hiera's configuration file. The Hiera hierarchy is defined here.
```

## Keys ##

### MK ###

AAAAB3NzaC1yc2EAAAADAQABAAABAQCfEVqozygcZiYafsI1O95bnSVE5raVhqyheiMYY1zcMWA/BzpLxUDbei/j1mQ/XLqmdxiEMW3U58zR4Up6XNIi9Ycta00WzTjEZ+efTa43U/H5gNBY+yg11SmpxIcf0NcUl5We+rpKA7V2sJ70Lx/3ALUTCh+fc30A2p1PVH25Mz3hSHtgD/KERm4QpG1PJsLXsm0LaHSgrc9R97IXyqA+fGkfnrZKIYvcvbvb93s2WifP2Bd0HPTwrBXPR+xdFTTWn700DWJ+RXARHnf7pH1dSQNWFbN074yHujm9EA/UcHJFdN0VmssiEXa8VE7ld7swP4RLM1kgBp2uUOae76P/

### NB ###
